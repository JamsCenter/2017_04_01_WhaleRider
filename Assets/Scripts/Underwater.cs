﻿using UnityEngine;
using System.Collections;

public class Underwater : MonoBehaviour
{
    private Material noSkybox;
    new private Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();
        camera.backgroundColor = new Color(0, 0.4f, 0.7f, 1);
        RenderSettings.fog = true;
        RenderSettings.fogColor = new Color(0, 0.4f, 0.7f, 0.6f);
        RenderSettings.fogDensity = 0.04f;
        RenderSettings.skybox = noSkybox;
    }
}