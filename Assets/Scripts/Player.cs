﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [SerializeField]
    private float playerSpeed = 2f;

    [SerializeField]
    private float rotateSpeed = 3.0F;

    [SerializeField]
    [Range(1, 4)]
    private int playerNumber = 1;

    [SerializeField]
    private GameObject mucus;

    [SerializeField]
    private float maxPlayerSize;

    [SerializeField]
    private GameObject blood;

    [SerializeField]
    private GameObject playerCamera;

    [SerializeField]
    private float shieldTime = 5f;

    private bool shielded;
    private Vector3 initialScale;
    private float playerSize;
    private float helpSize;
    private Vector3 moveDirection = Vector3.zero;
    private bool playingSplash = false;

    private string JoyHorizontal
    {
        get
        {
            return "Joy" + playerNumber.ToString() + "Horizontal";
        }
    }
    public float dashSpeed = 10;
    bool animating = false;
    bool dashing = false;
    float dashingCoolDown = 4f;
    float dashingTimer = 0;
    float dashingEnergyGain = 0;
    private bool deactivatingMucus;
    private float initialSize;

    private string JoyVertical
    {
        get
        {
            return "Joy" + playerNumber.ToString() + "Vertical";
        }
    }

    private string ActionButton
    {
        get
        {
            return "ActionButton" + playerNumber.ToString();
        }
    }

    void Start()
    {
        initialScale = transform.localScale;
        playerSize = 1;
        initialSize = playerSize;
        helpSize = 1;
        shielded = false;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("food"))
        {
            if (playerSize < maxPlayerSize)
            {
                var planktonEnergy = collider.gameObject.GetComponent<PlanktonScript>().GetEnergy();
                EatFood(planktonEnergy / 100);
                Destroy(collider.gameObject);
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!shielded)
            {
                AudioManager.Instance.PlayBubbleGrow();
                mucus.SetActive(true);

                if (helpSize < 2)
                {
                    helpSize += 0.5f;
                }
                ResizePlayer();
                shielded = true;
                StartCoroutine(DeactivateMucus());
            }
        }        

        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Whale"))
		{

            if(playerSize >= maxPlayerSize && helpSize > 1)
            {
                Instantiate(blood, collision.gameObject.transform.position, collision.gameObject.transform.rotation);                
				if(collision.gameObject.GetComponent<WhaleDeath>()!=null)
               	 collision.gameObject.GetComponent<WhaleDeath>().DieBitch();
            }
            AudioManager.Instance.PlayGotHit();
            playerSize -= 0.5f;
			if(blood)
            Instantiate(blood, transform.position, transform.rotation);
            GotHit();
            ResizePlayer();
        }
    }

    private void GotHit()
    {        
        if((helpSize <= 1 && !shielded)|| (playerSize < initialSize))
        {
            playerCamera.GetComponent<CameraFollow>().targets.Remove(gameObject);
            Destroy(gameObject);
            GameScript.current.CheckEndConditions();
        } else
        {
            if(!shielded) StartCoroutine(IsInvincible());
            StartCoroutine(PushBack());
        }
    }

    private IEnumerator IsInvincible()
    {
        shielded = true;
        yield return new WaitForSeconds(2f);
        shielded = false;
    }

    private IEnumerator PushBack()
    {
        float time = 0.25f;

        while (time > 0) {
            GetComponent<Rigidbody>().velocity = transform.TransformDirection(Vector3.back) * Time.deltaTime * 1500;            
            yield return null;
            time -= Time.deltaTime;
        }
    }

    //void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        StartCoroutine(DeactivateMucus());            
    //    }
    //}

    private IEnumerator DeactivateMucus()
    {
        Debug.Log("Deactivating");
        yield return new WaitForSeconds(shieldTime);
        AudioManager.Instance.PlayBubbleShrink();
        helpSize -= 0.2f;
        if (helpSize < 1)  helpSize = 1;
        ResizePlayer();
        mucus.SetActive(false);
        shielded = false;
        Debug.Log("Deactivated");
    }

    void EatFood(float energy)
    {

        AudioManager.Instance.PlayChomp();
        
        if (!dashing)
        {
            playerSize += energy;
            playerSpeed += energy / 5;
            ResizePlayer();
        }
        else
            dashingEnergyGain += energy;
    }

    IEnumerator dash()
    {
        if ( !dashing && dashingTimer <= 0 )
        {
            dashing = true;
            dashingEnergyGain = 0;
            float baseSize = playerSize;
            float targetSize = playerSize - playerSize/4f;
            float baseSpeed = playerSpeed;
            float targetSpeed = playerSpeed + dashSpeed ;
            float duration = 1.5f;
            float elapsed = 0f;
            while (elapsed < duration )
            {
                playerSize = Mathf.Lerp(baseSize, targetSize, elapsed / duration);
                playerSpeed = Mathf.Lerp(baseSpeed, targetSpeed, elapsed / duration);
                ResizePlayer();
                elapsed += Time.deltaTime;
                if (elapsed > duration) elapsed = duration;
                yield return 0;
            }
            elapsed = 0;
            while (elapsed < duration)
            {
                playerSpeed = Mathf.Lerp(targetSpeed, baseSpeed, elapsed / duration);
                elapsed += Time.deltaTime;
                if (elapsed > duration) elapsed = duration;
                yield return 0;
            }
            dashing = false;
            dashingTimer = dashingCoolDown;
            EatFood(dashingEnergyGain);
        }

    }

    private void ResizePlayer()
    {
        Vector3 newSize = initialScale * Mathf.Clamp(playerSize * helpSize, 0, maxPlayerSize);
        transform.localScale = newSize;
    }
    
    void Update ()
    {
        moveDirection = new Vector3(Input.GetAxis(JoyHorizontal), 0, Input.GetAxis(JoyVertical) * -1);

        if (moveDirection != Vector3.zero)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection, Vector3.up), Time.deltaTime * playerSpeed);
            //transform.rotation = Quaternion.LookRotation(moveDirection);
            if (!playingSplash)
            {
                StartCoroutine(PlayingSplash());
            }

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection, Vector3.up), Time.deltaTime * playerSpeed/2);
        }
        if (Input.GetButtonDown(ActionButton))
        {
            StartCoroutine( dash() );

       }
        else if(dashingTimer > 0f ) dashingTimer -= Time.deltaTime;

        moveDirection *= playerSpeed;
                
        GetComponent<Rigidbody>().velocity = moveDirection * Time.deltaTime * playerSpeed;
    }

    private IEnumerator PlayingSplash()
    {
        playingSplash = true;
        AudioManager.Instance.PlaySwims();
        yield return new WaitForSeconds(4f);
        playingSplash = false;
    }
}
