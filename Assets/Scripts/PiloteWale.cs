﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiloteWale : MonoBehaviour {

	public InputController _controllers;

	public Transform _seatRoot;
	public WandController _left;
	public WandController _right;


	public float _speedFactorByTriggers=6;


	[Header("Debug")]
	public float _angle;

	public Vector3 startPressingCenterPoint;

	public Vector3 _direction ;

	public float _speedFactor;



	public Vector3 _startCenterPosition;
	public Vector3 _currentCenerPosition;
	public Vector3 _directionFromStart;



	void Start () {
		if(_left==null)
	    	_left = _controllers.left;

		if(_right==null)
     		_right = _controllers.right;
	}

	void Update () {


		_angle = GetAngleOf(_left.transform, _right.transform, _seatRoot);

		if ( !_left.triggerPress && ! _right.triggerPress) {

//			_currentCenerPosition = GetCenterOfCompareTo (_seatRoot, _left, _right);
			if (_startCenterPosition == Vector3.zero) {
				_startCenterPosition = _currentCenerPosition;
			}

			_direction = Vector3.forward;
			_speedFactor=1;

		} else {
			_direction.z =1f;
			_direction.y =0f;
			_direction.x = _angle/90f;
			_startCenterPosition = Vector3.zero;
			_speedFactor = _left.triggerAxis * _speedFactorByTriggers + _right.triggerAxis * _speedFactorByTriggers;
		}

	}

	public float GetSpeedFactor(){ return _speedFactor; }

//
	public float  GetAngleOf(Transform left, Transform right, Transform seat)
	{

		Vector3 leftHand = seat.InverseTransformPoint (left.position);
		Vector3 rightHand = seat.InverseTransformPoint (right.position);
		leftHand.z = 0;
		rightHand.z = 0;

		Debug.DrawLine (leftHand, rightHand, Color.green);

		Debug.DrawLine (Vector3.zero, leftHand, Color.red);
		Debug.DrawLine (Vector3.zero, rightHand, Color.red);

		Vector3 orthoPoint = rightHand-leftHand;
		float angle =- Mathf.Clamp(Vector3.Angle (Vector3.right, orthoPoint),-90f,90f) * Mathf.Sign( orthoPoint.y );

	

		return angle;
	}
}
