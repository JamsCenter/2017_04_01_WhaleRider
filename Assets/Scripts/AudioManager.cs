﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager Instance;

    [SerializeField]
    private AudioClip chompFX;
    [SerializeField]
    private AudioClip bubbleGrowFX;
    [SerializeField]
    private AudioClip bubbleShrinkFX;
    [SerializeField]
    private AudioClip swimsFX;
    [SerializeField]
    private AudioClip gotHitFX;

    private AudioSource audioSource;

    void Start()
    {
        if(Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        audioSource = GetComponent<AudioSource>();
    }
    public void PlayChomp()
    {
        audioSource.PlayOneShot(chompFX);
    }
    public void PlayBubbleGrow()
    {
        audioSource.PlayOneShot(bubbleGrowFX);
    }
    public void PlayBubbleShrink()
    {
        audioSource.PlayOneShot(bubbleShrinkFX);
    }
    public void PlaySwims()
    {
        audioSource.PlayOneShot(swimsFX, 0.5f);
    }

    internal void PlayGotHit()
    {
        audioSource.PlayOneShot(gotHitFX, 0.5f);
    }
}
