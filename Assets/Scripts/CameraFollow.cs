﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public List<GameObject> targets;
    
    private float initRatio;
    private bool animatingPosition = false;
    private Vector3 targetPosition;

    void Start()
    {
        initRatio = gameObject.transform.position.y / GetBiggestDistance();
    }

	void Update ()
    {   
        FollowTargets();
        UpdateZoom();
	}

    private void UpdateZoom()
    {
        float height = GetBiggestDistance() * initRatio;
        height = Mathf.Clamp(height, 35f, 150f);
        if (!animatingPosition)
            StartCoroutine(animatePosition(height));
    }


    IEnumerator animatePosition(float targetY, float duration = 1f)
    {
        if (!animatingPosition)
        {
            animatingPosition = true;
            float elapsed = 0f;
            Vector3 baseValue = transform.position;
            while (elapsed < duration)
            {
                transform.position = Vector3.Lerp(baseValue, new Vector3(targetPosition.x , targetY, targetPosition.z ), elapsed / duration);
                elapsed += Time.deltaTime;
                if (elapsed > duration) elapsed = duration;
                yield return 0;
            }
            animatingPosition = false;
        }
    }

    private float CalculateTargetsTotalDistance()
    {
        float calculatedDistanceX = 0;
        for (var i = 0; i < targets.Count - 2; i++)
        {
            calculatedDistanceX += Vector3.Distance(targets[i].transform.position, targets[i + 1].transform.position);
        }

        return calculatedDistanceX ;
    }

    private float GetBiggestDistance()
    {
        float result =1f;
        List<float> dist = new List<float>();
        if (targets.Count >= 2)
        {
            dist.Add(Vector3.Distance(targets[0].transform.position, targets[1].transform.position));
            if (targets.Count >= 3)
            {
                dist.Add(Vector3.Distance(targets[0].transform.position, targets[2].transform.position));
                dist.Add(Vector3.Distance(targets[1].transform.position, targets[2].transform.position));
            }
            if (targets.Count >= 4)
            {
                dist.Add(Vector3.Distance(targets[1].transform.position, targets[3].transform.position));
                dist.Add(Vector3.Distance(targets[2].transform.position, targets[3].transform.position));
                dist.Add(Vector3.Distance(targets[0].transform.position, targets[3].transform.position));
            }
            dist.Sort();
            result = dist[dist.Count - 1];
        }
        return result;
    }


    private void FollowTargets()
    {
        var center = Vector3.zero;
        var numTargets = targets.Count;

        foreach (var target in targets)
        {
            if (target != null)
            {
                center += target.transform.position;
            } else
            {
                numTargets--;
            }
        }

        if (numTargets == 0) return;
        var finalCenter = center / numTargets;

        targetPosition = new Vector3(finalCenter.x, 1, finalCenter.z);
    }    
}
