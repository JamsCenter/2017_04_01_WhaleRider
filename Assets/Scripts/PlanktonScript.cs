﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanktonScript : MonoBehaviour
{
    FoodDistributionScript fds;
    public float energy;
    
    float coolDown;
    Vector3 direction;
    float timer;
    
	public void Init(FoodDistributionScript _manager)
    {
        fds = _manager;
        ChangeDirection();
        timer = coolDown * Random.Range(0f, 1f);
    }

    private void ChangeDirection()
    {
        direction = Vector3.zero;
        direction.x = Random.Range(-1f, 1f);
        direction.y = 0f;
        direction.z = Random.Range(-1f, 1f);
    }

    public void SetEnergy(float amount)
    {
        energy = amount;
        coolDown = amount * 0.1f;
    }

    public float GetEnergy()
    {
        return energy;
    }

    private void Update()
    {
        transform.localScale = Vector3.one * Mathf.Max( fds.minScale, energy * fds.scaleFactor );

        if (timer <= 0 )
        {
            transform.localPosition = transform.localPosition + direction * Time.deltaTime;
            if ( timer < -2)
            {
                ChangeDirection();
                timer = coolDown;
            }
        }
        timer -= Time.deltaTime;
    }
}
