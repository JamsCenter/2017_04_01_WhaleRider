﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodDistributionScript : MonoBehaviour
{
    public int planktonCount = 100;
    public GameObject planktonPrefab;
    public List<GameObject> planktonMeshes;
    [Range(0.05f, 1f)]
    public float minScale = 0.1f;

    [Range(0.01f, 0.1f)]
    public float scaleFactor = 0.01f;
    float energyLeft;
	public void Init()
    {
        energyLeft = GameScript.current.totalEnergy;

        float maxEnergyAmount = energyLeft/planktonCount;
        float energyAmount;

        GameObject tmpGo;
        Vector3 tmpV3;
        for ( int i =0; i< planktonCount; i++)
        {
            tmpGo = Instantiate(planktonMeshes[i% planktonMeshes.Count]);
            tmpGo.transform.SetParent(transform);
            tmpV3 = Vector3.zero;
            tmpV3.x = Random.Range(-GameScript.current.playingAreaSizeX, GameScript.current.playingAreaSizeX);
            tmpV3.y = transform.position.y;
            tmpV3.z = Random.Range(-GameScript.current.playingAreaSizeY, GameScript.current.playingAreaSizeY);

            tmpGo.transform.position = tmpV3;
            tmpGo.GetComponent<PlanktonScript>().Init(this);
            energyAmount = Random.Range( Mathf.Min(0.1f, energyLeft), Mathf.Min(maxEnergyAmount, energyLeft));
            energyLeft -= energyAmount;
            tmpGo.GetComponent<PlanktonScript>().SetEnergy(energyAmount);
        }
    }
}
