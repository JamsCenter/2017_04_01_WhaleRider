﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayersScript : MonoBehaviour
{
    [SerializeField]
    private GameObject winFrame;
    [SerializeField]
    private GameObject loseFrame;


    public DuckScript duck;



    public void Init()
    {
        WinScreenVisibility(false);
        LoseScreenVisibility(false);
    }

    public void WinScreenVisibility( bool visible)
    {
        StartCoroutine(animateScreenVisibility(winFrame, visible, .75f));
        if (visible)
        {
            StartCoroutine(duck.Animate() );
            StartCoroutine(duck.playSound(0.4f) );
        }
    }

    public void LoseScreenVisibility(bool visible)
    {
        StartCoroutine(animateScreenVisibility(loseFrame, visible, .75f));
    }

    IEnumerator animateScreenVisibility(GameObject screen, bool visible, float duration = 1f)
    {
        float elapsed = 0f;

        Graphic[] graphics = screen.GetComponentsInChildren<Graphic>();
        Dictionary<Graphic, Color> baseColors = new Dictionary<Graphic, Color>();
        Dictionary<Graphic, Color> targetColors = new Dictionary<Graphic, Color>();
        foreach (Graphic g in graphics)
        {
            baseColors.Add(g, g.color);
            targetColors.Add(g, new Color(g.color.r, g.color.g, g.color.b, visible ? 1f : 0f ));
        }
        while (elapsed < duration)
        {
            foreach (Graphic g in graphics)
            {
                g.color = Color.Lerp(baseColors[g], targetColors[g], elapsed / duration);
            }
            elapsed += Time.deltaTime;
            if (elapsed > duration) elapsed = duration;
            yield return 0;
        }
        foreach (Graphic g in graphics)
        {
            g.color = targetColors[g];
        }
    }

}
