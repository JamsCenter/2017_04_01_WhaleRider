﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScript : MonoBehaviour
{   
    private static GameScript Current; 
    public static GameScript current
    {
        get
        {
            if (Current == null) Current = GameObject.Find("/Game").GetComponent<GameScript>();
            return Current;
        }
    }

    [Header("Game")]
    public CameraFollow camFollow;
    public FoodDistributionScript foodRoot;
    public int totalEnergy = 20000;
    public int playingAreaSizeX = 100;
    [Space()]
    [Header("UI")]
    public UIPlayersScript UIPlayers;

    public float playingAreaSizeY {
        get {
            return playingAreaSizeX;
        }
    }
    
    void Start()
    {
        foodRoot.Init();
        UIPlayers.Init();
    }

    void Update()
    {
        
    }

    public void PlayersWin()
    {
        UIPlayers.WinScreenVisibility(true);
    }

    public void CheckEndConditions()
    {
        if (camFollow.targets.Count == 1)
        {
            UIPlayers.LoseScreenVisibility(true);
        }
    }

}
