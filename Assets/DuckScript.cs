﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuckScript : MonoBehaviour
{
    public List<RectTransform> targets;
    public AudioClip kwiKwik;
    public AudioSource audio;

    RectTransform rect;
    bool animatingPosition = false;
    
    void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    public IEnumerator playSound( float delay )
    {
        yield return new WaitForSeconds(delay);
        audio.PlayOneShot(kwiKwik);
    }


    public IEnumerator Animate()
    {
        if (!animatingPosition)
        {
            animatingPosition = true;
            float elapsed = 0f;
            Vector3 baseValue = rect.anchoredPosition;
            float duration = 0.8f;

            foreach (RectTransform targ in targets)
            {
                while (elapsed < duration)
                {
                    if( elapsed > duration * 0.33f) 

                    rect.anchoredPosition = Vector3.Lerp(baseValue, targ.anchoredPosition, elapsed / duration);
                    elapsed += Time.deltaTime * 0.5f;
                    if (elapsed > duration) elapsed = duration;
                    yield return 0;
                }
                rect.anchoredPosition = targ.anchoredPosition;

                yield return new WaitForSeconds(0.2f);

                elapsed = 0f;
                duration = 0.15f;
                baseValue = rect.anchoredPosition;
            }

            animatingPosition = false;
        }
    }
}
