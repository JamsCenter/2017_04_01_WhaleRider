﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMouth : MonoBehaviour {


	public Animator _animator;


	public float _mouthOpen =1f;

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.CompareTag("food") || collider.gameObject.CompareTag("Player"))
		{
			_mouthOpen = 1f;
			_animator.SetBool ("OpenMouth",true);

		}
	}

	public void Update(){

		if(_mouthOpen>0f){
			_mouthOpen -= Time.deltaTime;
			_animator.SetBool ("OpenMouth",false);

		}

	}

}
