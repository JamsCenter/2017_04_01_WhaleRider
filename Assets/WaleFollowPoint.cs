﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaleFollowPoint : MonoBehaviour {


	public float _followSpeedRatio=2;
	public float _followRotationRatio=1;
	public Transform _followPoint;
	public Transform _waleRoot;


	void Update () {
		if (_followPoint) {
			transform.position = Vector3.Lerp (transform.position, _followPoint.position, Time.deltaTime*_followSpeedRatio);
			transform.rotation = Quaternion.Lerp (transform.rotation, _followPoint.rotation, Time.deltaTime*_followRotationRatio);
		}
	}
}
